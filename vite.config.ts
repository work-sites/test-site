import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
const config = defineConfig({
  plugins: [vue()],
  server: {
    proxy: {
      '/api': "http://127.0.0.1:3000"
    }
  },
})

export default config


