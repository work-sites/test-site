const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('test', 'postgres', 'spd', {
    host: 'localhost',
    dialect: 'postgres',
});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
}, {timestamps: false});

const Article = sequelize.define('article', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.TEXT
    },
    creationDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
});

const Test = sequelize.define('test', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    }
});

const Question = sequelize.define('question', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

const Answer = sequelize.define('answer', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    },
    score: {
        type: Sequelize.INTEGER
    }
});

const Result = sequelize.define('result', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    score: {
        type: Sequelize.INTEGER
    },
    description: {
        type: Sequelize.STRING
    }
});

const TestResult = sequelize.define('test_result', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

const Comment = sequelize.define('comment', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: Sequelize.STRING
    }
});

Test.hasMany(Question);
Question.belongsTo(Test);

Test.hasMany(Result);
Result.belongsTo(Test);

Question.hasMany(Answer);
Answer.belongsTo(Question);

Article.hasMany(Comment);
Comment.belongsTo(Article);

Comment.belongsTo(User);
User.hasMany(Comment);

User.hasMany(TestResult)
Test.hasMany(TestResult)
TestResult.belongsTo(User)
TestResult.belongsTo(Test)

module.exports = {
    User,
    Article,
    Test,
    Question,
    Answer,
    Result,
    TestResult,
    Comment
};

