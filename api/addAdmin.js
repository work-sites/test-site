const { User, Article, Test, Question, Answer, Result, TestResult, Comment } = require('./database')

const bcrypt = require('bcrypt')
const saltRounds = 10

const CreateAdmin = async (login, password) => {
    let hashedPassword = await bcrypt.hash(password, saltRounds)
    let user = await User.create({
        name: login,
        password: hashedPassword,
        isAdmin: true
    })
    return user.toJSON()
}

console.log(CreateAdmin('unbanished', 'spd'))