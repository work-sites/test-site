const { User, Article, Test, Question, Answer, Result, TestResult, Comment } = require('./database')

const test_function = async () => {

    let user = await User.findOne({ where: { id: 1} })

    let article = await Article.findOne({ where: { id: 1 } })

    let comment = await Comment.create({
        description: 'Test comment',
        userId: user.id,
        articleId: article.id
    })

    let data = await Article.findAll({ include: [{
        model: Comment,
        include: [User]
    }], where: { id: 1 } })

    let json_data = data.map(item => {
        return {
            item: item.toJSON(),
            comments: item.comments.map(comment => comment.toJSON())
        }
    })

    console.dir(json_data, { depth: null })

}

test_function()