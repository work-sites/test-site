const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.use(bodyParser.json())
const PORT = process.env.PORT || 3000

const bcrypt = require('bcrypt')
const saltRounds = 10

const jwt = require('jsonwebtoken')

const { User, Article, Test, Question, Answer, Result, TestResult, Comment } = require('./database')

app.post('/api/register', async (req, res) => {

    // Check if data is not empty
    if (!req.body.login || !req.body.email || !req.body.password) {
        console.log(req.body)
        res.status(400).send('Incomplete data')
        return
    }

    // Hash password
    const hash = await bcrypt.hash(req.body.password, saltRounds)
    req.body.password = hash

    // Check if email and login are unique
    let isEmailNotUnique = await User.findOne({ where: { email: req.body.email } })

    if (isEmailNotUnique) {
        res.status(400).send('Email already exists')
        return
    }

    let isLoginNotUnique = await User.findOne({ where: { name: req.body.login } })

    if (isLoginNotUnique) {
        res.status(400).send('Login already exists')
        return
    }

    // Create user
    let user = await User.create({
        name: req.body.login,
        email: req.body.email,
        password: req.body.password
    })

    // Create token
    let token = jwt.sign({ id: user.id }, 'secret')
    res.send({token})
})

app.post('/api/login', async (req, res) => {
    if (!req.body.login || !req.body.password) {
        res.status(400).send('Incomplete data')
        return
    }
    let user = await User.findOne({ where: { name: req.body.login } })
    if (!user) {
        res.status(400).send('Incorrect login or password')
        return
    }
    if (!await bcrypt.compare(req.body.password, user.password)) {
        res.status(401).send('Incorrect login or password')
        return
    }
    const token = jwt.sign({ id: user.id }, 'secret')
    res.send({token})
})

app.get('/api/profile', async (req, res) => {

    let token = req.headers.authorization

    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }

    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ 
        where: { id: decoded.id }, 
        include: [{
            model: TestResult,
            include: [{ model: Test }]
        }],
        attributes: { exclude: ['password'] } 
    })

    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    res.send(user.toJSON())
})

app.post('/api/test/result', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }
    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }

    if(!req.body.testId){
        res.status(400).send('Incomplete data')
        return
    }
    let test = await Test.findOne({ where: { id: req.body.testId } })
    if (!test) {
        res.status(400).send('Incorrect test id')
        return
    }

    if(!req.body.description){
        res.status(400).send('Incomplete data')
        return
    }

    let result = await TestResult.create({
        description: req.body.description,
        userId: user.id,
        testId: test.id
    })

    res.send({id: result.id})
})

app.get('/api/test/results', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }
    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    let results = await TestResult.findAll({ 
        include: [Test], 
        where: { userId: user.id } 
    })
    res.send(results.toJSON())
})

app.post('/api/admin_panel', async (req, res) => {
    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    if (user.isAdmin === false) {
        res.status(400).send('Incorrect token')
        return
    }
    
    let articles = await Article.findAll()

    let artilcles_data = articles.map(article => {
        return {
            id: article.id,
            title: article.title,
            description: article.description,
            date: article.creationDate
        }
    })

    let tests = await Test.findAll()

    let tests_data = tests.map(test => {
        return {
            id: test.id,
            name: test.name,
            description: test.description,
            date: test.creationDate
        }
    })

    res.send({articles: artilcles_data, tests: tests_data, isAdmin: user.isAdmin})

})

app.get('/api/articles', async (req, res) => {
    let articles = await Article.findAll()

    let artilcles_data = articles.map(article => {
        return {
            id: article.id,
            title: article.title,
            description: article.description,
            date: article.creationDate
        }
    })
    res.send({articles: artilcles_data})
})

app.get('/api/tests', async (req, res) => {
    let tests = await Test.findAll()

    let tests_data = tests.map(test => {
        return {
            id: test.id,
            name: test.name,
            description: test.description
        }
    })
    res.send({tests: tests_data})
})

app.delete('/api/test/:id', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    if (user.isAdmin === false) {
        res.status(400).send('Incorrect token')
        return
    }

    if(!req.params.id) {
        res.status(400).send('Incomplete data')
        return
    }
    await Test.destroy({ where: { id: req.params.id } })
    res.send('ok')
})

app.post('/api/article', async (req, res) => {

    if (!req.body.title || !req.body.description || !req.body.content) {
        res.status(400).send('Incomplete data')
        return
    }

    let article = await Article.create({
        title: req.body.title,
        description: req.body.description,
        content: req.body.content
    })
    res.send({id: article.id})
})

app.post('/api/comment', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }
    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }

    if(!req.body.article_id || !req.body.description) {
        res.status(400).send('Incomplete data')
        return
    }

    let comment = await Comment.create({
        description: req.body.description,
        userId: user.id,
        articleId: req.body.article_id
    })
    res.send('ok')
})

app.delete('/api/comment/:id:user_id', async (req, res) => {
    if(!req.params.id || !req.params.user_id) {
        res.status(400).send('Incomplete data')
        return
    }

    let comment = await Comment.findOne({ where: { id: req.params.id, userId: req.params.user_id } })
    if (!comment) {
        res.status(400).send('Incorrect comment id')
        return
    }
    await comment.destroy()
    res.send('ok')
})

app.get('/api/article/:id', async (req, res) => {
    let id = req.params.id

    let article = await Article.findOne({ 
        where: { id: id }, 
        include: { 
            model: Comment, include: { 
                model: User, 
                attibutes: { exclude: ['password']} 
            } 
        } 
    })

    if (!article) {
        res.status(400).send('Incorrect article id')
        return
    }

    res.send({article: article.toJSON()})
})

app.delete('/api/article/:id', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    if (user.isAdmin === false) {
        res.status(400).send('Incorrect token')
        return
    }

    if(!req.params.id) {
        res.status(400).send('Incomplete data')
        return
    }

    let id = req.params.id
    let article = await Article.findOne({ where: { id: id } })
    if (!article) {
        res.status(400).send('Incorrect article id')
        return
    }
    await article.destroy()
    res.send('ok')
})

app.delete('/api/result/:id', async (req, res) => {

    console.log(req.params.id)

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    
    if(!req.params.id) {
        res.status(400).send('Incomplete data')
        return
    }

    let id = req.params.id
    let result = await TestResult.findOne({ where: { id: id, userId: decoded.id } })
    if (!result) {
        res.status(400).send('Incorrect result id')
        return
    }
    await result.destroy()
    res.send('ok')
})

app.post('/api/test', async (req, res) => {

    let token = req.headers.authorization
    if (!token) {
        res.status(400).send('Incomplete data')
        return
    }
    token = token.split(' ')[1]

    let decoded = jwt.verify(token, 'secret')
    if (!decoded) {
        res.status(400).send('Incorrect token')
        return
    }

    let user = await User.findOne({ where: { id: decoded.id } })
    if (!user) {
        res.status(400).send('Incorrect token')
        return
    }
    if (user.isAdmin === false) {
        res.status(400).send('Incorrect token')
        return
    }

    if (!req.body.name || !req.body.description || !req.body.questions) {
        res.status(400).send('Incomplete data')
        return
    }

    let test = await Test.create({
        name: req.body.name,
        description: req.body.description,
    })

    for (let question of req.body.questions) {

        let new_question = await Question.create({
            description: question.description,
            testId: test.id
        })

        for (let answer of question.answers) {
            await Answer.create({
                description: answer.description,
                score: answer.score,
                questionId: new_question.id
            })
        }
    }

    for(let result of req.body.results) {
        await Result.create({
            description: result.description,
            score: result.score,
            testId: test.id
        })
    }

    res.send('ok')
})

app.get('/api/test/:id', async (req, res) => {

    if(!req.params.id) {
        res.status(400).send('Incomplete data')
        return
    }

    let id = req.params.id
    let test = await Test.findOne({ 
        where: { id: id }, 
        include: [
            { 
                model: Question, 
                include: { model: Answer } 
            }, 
            Result
        ] 
    })

    if (!test) {
        res.status(400).send('Incorrect test id')
        return
    }
    res.send({test: test.toJSON()})
})

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})