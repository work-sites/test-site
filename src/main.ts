import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Main from './components/Main.vue'
import Register from './components/Register.vue'
import Authorise from './components/Authorise.vue'
import Profile from './components/Profile.vue'
import AdminPanel from './components/AdminPanel.vue'
import AdminCreateArticle from './components/AdminCreateArticle.vue'
import AdminCreateTest from './components/AdminCreateTest.vue'
import Article from './components/Article.vue'
import Articles from './components/Articles.vue'
import Test from './components/Test.vue'
import Tests from './components/Tests.vue'
import { createRouter, createWebHistory } from 'vue-router'
import { VueShowdownPlugin } from 'vue-showdown'

const routes = [
    { path: '/', component: Main },
    { path: '/register', component: Register },
    { path: '/authorise', component: Authorise },
    { path: '/profile', component: Profile },
    { path: '/admin', component: AdminPanel },
    { path: '/admin/article', component: AdminCreateArticle },
    { path: '/article/:id', component: Article },
    { path: '/articles', component: Articles },
    { path: '/admin/test', component: AdminCreateTest },
    { path: '/test/:id', component: Test },
    { path: '/tests', component: Tests },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

const app = createApp(App)

app.use(VueShowdownPlugin, {
    options: {
        emoji: true
    },
    flavors: ['github']
})

app.use(router)

app.mount('#app')
